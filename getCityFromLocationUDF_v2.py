from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

import geopandas as gpd
from shapely.geometry import Point

gdf = gpd.read_file("/opt/fyrefuse/for_albar/Governorate.json")
gdf = gdf.to_crs(epsg=4326)

# given a set of coordinates the function returns the corresponding city's city_key 

def find_best_match_row_from_lat_long(gdf, lat, long):
    # Create a Point geometry using the given latitude and longitude
    try:
        point = Point(float(long), float(lat))
    except Exception as e:
        return ""
    
    # Check if the point intersects with any geometry
    containing_row = gdf[gdf['geometry'].intersects(point)]
    if not containing_row.empty:
        matched_row = containing_row.iloc[0]
        #return containing_row.iloc[0]
    else:
        # If no intersecting geometry found, find the closest geometry
        gdf['distance'] = gdf['geometry'].distance(point)
        matched_row = gdf.loc[gdf['distance'].idxmin()]
    
    try:
        out_field = matched_row['CITY_KEY']
    except Exception:
        out_field = ""

    return out_field


class getCityFromLocationUDF_v2():
    def __init__(self):
        super().__init__()
        print("getCityFromLocationUDF_v2 init")
        self.udf = udf(getCityFromLocationUDF_v2.getCity, StringType())

    @staticmethod
    def getCity(lat, lng):
        global gdf
        
        #city = "{} {}".format(lat, lng) #test
        city_key = find_best_match_row_from_lat_long(gdf, lat, lng)
        return f"{city_key}"
