import http.client
import pandas as pd
import json
import sys
from time import sleep
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *
from kafka import KafkaClient, KafkaProducer, TopicPartition, KafkaAdminClient
import ssl
import glob
import os
import re
import datetime as dt
import time
import shutil
import requests
import time
import csv

STATUS_RUNNING = "Running"
STATUS_FAILED = "Failed"
STATUS_STOPPED = "Stopped"
PIPELINE_LOGGER_MESSAGE_STATUS_END = "END PIPELINE"
PIPELINE_LOGGER_MESSAGE_STATUS_START = "START PIPELINE"
PIPELINE_LOGGER_STATUS_START = "START"
PIPELINE_LOGGER_STATUS_END = "END"
PIPELINE_LOGGER_STATUS_RUNNING = "RUNNING"
PIPELINE_LOGGER_STATUS_ERROR = "ERROR"
PIPELINE_LOGGER_STATUS_OK = "OK"
# to make conversion from JS to Py date string format
js_to_python_dt = {
    "YYYY": "%Y",
    "YY": "%y",
    "MM": "%m",
    "MMM": "%b",
    "MMMM": "%B",
    "DD": "%d"
}

def load_config_from_env():
    try:
        inputs = json.loads(spark.conf.get("spark.executerENV.inputs").replace("'", "\""))
        outputs = json.loads(spark.conf.get("spark.executerENV.outputs").replace("'", "\""))
        kafka_config = json.loads(spark.conf.get("spark.executerENV.kafka_config").replace("'", "\""))
        repo_params = json.loads(spark.conf.get("spark.executerENV.repo_credentials").replace("'", "\""))
        metadata = json.loads(spark.conf.get("spark.executerENV.metadata").replace("'", "\""))
        jobs = json.loads(spark.conf.get("spark.executerENV.jobs").replace("'", "\""))

        config = inputs
        #concat all the dict as one
        config.update(outputs)
        config.update(kafka_config)
        config.update(repo_params)
        config.update(metadata)
        config.update(jobs)

    except:
        config = False
    finally:
        return config

def load_config_from_file(filename):
    try:
        with open(filename) as f:
            conf = json.load(f)
    except:
        conf = False
    finally:
        return conf

def wait_and_reopen_connection_backoff(conn,host):
    i = 0
    while True:
        try:
            sleep(2**i) #backoff waiting
            print("Reopening connection after broker forced closing i = "+str(i))
            conn = http.client.HTTPSConnection(host)
            return conn
        except:
            print("error HTTPSConnection...retrying...")
            i += 1

def format_review(full_json_response,fields):
    review = {}
    #expected fields ["pros","cons","date"]
    for field in fields:
        field = field["attribute"]
        try:
            field_content = full_json_response[field]
        except:
            field_content = ""

        if type(field_content) == str:
            field_content = field_content.replace("\n"," ").replace("\r"," ").strip()

        review[field] = field_content # building a new clean dict

    """
    if ("pros" in review and "cons" in review and "date" in review):
        review_custom = {}
        review_custom["review_text"] = review["pros"]+" "+review["cons"]
        review_custom["review_date"] = review["date"]
        return review_custom
    """
    return review

def make_review_schema(fields):
    
    schema = StructType()
    for field in fields:
        field = field["attribute"]
        schema.add(field,StringType())

    return schema

def prepare_spark_select_fields(fields):
    select = ['facility_id']

    for field in fields:
        field = field["attribute"]
        select.append(field)

    return select

def rename_output_files(dir,output_prefix):
    for i,file in enumerate(glob.glob(dir+"part*.csv")):
        os.rename(file,dir+output_prefix+str(i)+".csv")

def remove_output_files(dir):
    try:
        os.remove(dir+"metadata")
        shutil.rmtree(dir+"sources")
        shutil.rmtree(dir+"offsets")
        shutil.rmtree(dir+"commits")
        shutil.rmtree(dir+"_spark_metadata")
        
        for file in glob.glob(dir+".*"):
            os.remove(file)
        for file in glob.glob(dir+"_*"):
            os.remove(file)    
        for file in glob.glob(dir+"_*.crc"):
            os.remove(file)
    except:
        print("failed to delete some temporary files")

def make_output_file_name(format):

    part_start_idx = format.find("_part")
    dt_format = format.split("_part")[0]

    dt_py_format = ""
    for sym in dt_format.split("-"):
        dt_py_format += js_to_python_dt[sym]+"-"
    dt_py_format = dt_py_format[:-1]

    string_dt = str(dt.datetime.now().strftime(dt_py_format))

    fname = string_dt+format[part_start_idx:part_start_idx+5]
    print(fname)
    return fname

def stop_stream_query(query, wait_time):
    """Stop a running streaming query"""
    while query.isActive:
        msg = query.status['message']
        data_avail = query.status['isDataAvailable']
        trigger_active = query.status['isTriggerActive']
        if not data_avail and not trigger_active and msg != "Initializing sources":
            print('Stopping query...')
            query.stop()
        time.sleep(5)

    # Okay wait for the stop to happen
    print('Awaiting termination...')
    query.awaitTermination(wait_time)

#percentage: between 0 - 100
#status could be: RUNNING, COMPLETED, SUCCEEDED, FAILED
def format_log_message(msg,percentage,status,ts):
    try:
        instance_id = config["metadata"]["instance_id"][0] #perchè arriva un array?
    except:
        instance_id = config["metadata"]["instance_id"]
    
    
    pipeline_id = config["metadata"]["pipeline_id"]
    log = {
        "key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*",
        "log_data": {
            "percentage": percentage,
            "version": "0.1",
            "status": status,
            "policies_id": "b'3LJLB\\xfd\\xb7\\x9d~\\xbc>s\\xb5\\x17\\xe6\\xf8'",
            "message": msg,
            "instance_id": instance_id,
            "job_configuration_id": config["jobs"][0],
            "timestamp": ts,
            "pipeline_id": pipeline_id
        }
    }
    return log

def send_log(log):
    #hostname = "localhost:8000"
    hostname = "fusilli-dev-2:8001"
    headers = {}
    headers["Content-Type"] = "application/json"
    json_string = json.dumps(log)

    response = requests.post(url='http://'+hostname+'/fusilli/logger/save-log/',
                            data=json_string, headers=headers)
    if response.status_code >= 400:
        return False
    else:
        return True

# ENTRYPOINT #
spark = SparkSession\
        .builder\
        .appName("Fusilli_Job-booking_facility_joiner")\
        .getOrCreate()

spark.sparkContext.setLogLevel("ERROR")

print(os.getlogin())

"""
config = load_config_from_file("./config.json")
if not config:
    print("Error opening config file")
    sys.exit(0)
"""

config = load_config_from_env()
if not config:
    msg = "Error opening config file."
    print(msg)
    log = format_log_message(msg = msg,
                            percentage = 10,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
    send_log(log)
    sys.exit(0)

msg = "Job is starting..."
print(msg)
log = format_log_message(msg = msg,
                         percentage = 10,
                         status = STATUS_RUNNING,
                         ts = int(time.time()*1000))
send_log(log)

#pwd = os.getcwd() #depends on where the spark-submit is launched
#pwd = "/home/subscriptions/tmp"
interesting_fields = config["inputs"][0]
rakuten_host = config["repositories"][0][0]["host"] # "booking-com.p.rapidapi.com" 
rakuten_token = config["repositories"][0][0]["token"]
page_upper_limit = 100000
output_dir = config["outputs"][0][0]["config"]["repository"]["schema"] #absolute path
output_dir = output_dir if output_dir[-1] == "/" else output_dir+"/" #add / to the end

try:
    instance_id = config["metadata"]["instance_id"][0] #perchè arriva un array?
except:
    instance_id = config["metadata"]["instance_id"]

match = re.search("(YY)*-*(M)*-+DD_part.*",config["outputs"][0][0]["config"]["entity"]) #check output format
if not match:
    msg = "Output file format not recognised..."
    print(msg)
    log = format_log_message(msg = msg,
                            percentage = 10,
                            status = STATUS_FAILED,
                            ts = int(time.time()*1000))
    send_log(log)
    sys.exit(0)

#loading lookup table
facility_file = output_dir+"Match BookingID with FacilityID v.0.2.xlsx"
lookup_df = pd.read_excel(facility_file, usecols=["booking_id","facility_ID"]) 
lookup_df = lookup_df.dropna()

#initializing kafka producer
# TODO: implement authentication
try:
    producer = KafkaProducer(bootstrap_servers=[config["broker_configuration"][0]["url"]+":"+str(config["broker_configuration"][0]["port"])],
                            value_serializer=lambda x: 
                            json.dumps(x).encode('utf-8'))
except:
    msg = "Kafka connetion failed. Aborting job..."
    print(msg)
    log = format_log_message(msg = msg,
                            percentage = 10,
                            status = STATUS_FAILED,
                            ts = int(time.time()*1000))
    send_log(log)
    sys.exit(0)

now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
print(now+": Kafka connetion established.")

#pipeline_id + instance_id are enough for uniqueness
topic = "fusilli_queue-"+str(config["metadata"]["pipeline_id"])+"-"+str(instance_id)

#connecting to rakuten
rakuten_request_headers = {
    'x-rapidapi-key': rakuten_token,
    'x-rapidapi-host': rakuten_host
}

try:
    try:
        conn = http.client.HTTPSConnection(rakuten_host)
    except:
        try:
            conn = http.client.HTTPSConnection(rakuten_host,context = ssl._create_unverified_context())
        except:
            msg = now+": rakuten connection failed."
            print(msg)
            log = format_log_message(msg = msg,
                                    percentage = 15,
                                    status = STATUS_FAILED,
                                    ts = int(time.time()*1000))
            send_log(log)
            sys.exit(0)

        msg = now+":Unverified context created."
        print(msg)
        log = format_log_message(msg = msg,
                                percentage = 15,
                                status = STATUS_RUNNING,
                                ts = int(time.time()*1000))
        send_log(log)
except:
    msg = now+": rakuten connection failed."
    print(msg)
    log = format_log_message(msg = msg,
                            percentage = 15,
                            status = STATUS_FAILED,
                            ts = int(time.time()*1000))
    send_log(log)
    sys.exit(0)

print(now+": rakuten connection established.")
#retrieving all the reviews for all the matched hotels and
#saving them in the kafka queue
total_extractions = 0
for idx,row in lookup_df[9:10].iterrows():

    hotel_id = row["booking_id"]
    print("Getting hotel: "+str(idx)+" - id: "+str(hotel_id))

    for page in range(page_upper_limit): #retrieving all the results

        query = "/v1/hotels/reviews?locale=en-gb&sort_type=SORT_RECENT_DESC&hotel_id=%s&page_number=%s"%(hotel_id,page)
        sleep(0.15) #0.15s every call

        try:
            conn.request("GET", query, headers=rakuten_request_headers)
            res = conn.getresponse()
        except Exception as e:
            #print("Request: "+str(e))
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = "Rakuten connection drop..."
            print(now+": "+msg)
            conn.close()

            conn = wait_and_reopen_connection_backoff(conn,rakuten_host) #it waits until connection is reopened
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = "Connection restablished."
            print(now+": "+msg)
            page -= 1
            continue

        if res.getcode() != 200: #call failed, skipping page
            continue
        
        try:
            data = res.read()
            json_data = json.loads(data)
        except:
            print("error json")
            continue

        n_hotels = len(json_data["result"])

        if(n_hotels > 0):
            total_extractions += n_hotels
            for i in range(n_hotels):

                review = format_review(json_data["result"][i],interesting_fields)

                try:
                    producer.send(topic, value=review)
                except Exception as e:
                    now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                    print(now+": "+str(e))
            try:
                pid = os.getpid()
                print(pid)
            except:
                conn.close() #end of retrieving process
                producer.close()
                client = KafkaClient(bootstrap_servers=config["broker_configuration"][0]["url"]+":"+str(config["broker_configuration"][0]["port"]))
                admin_client = KafkaAdminClient(bootstrap_servers=config["broker_configuration"][0]["url"]+":"+str(config["broker_configuration"][0]["port"]))
                deletion = admin_client.delete_topics([topic])
                try:
                    future = client.cluster.request_update()
                    client.poll(future=future)
                except Exception as e:
                    print(str(e))
                finally:
                    client.close()
                    admin_client.close()

                now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                msg = now+": Job Stopped."
                log = format_log_message(msg = msg,
                                        percentage = 50,
                                        status = STATUS_STOPPED,
                                        ts = int(time.time()*1000))
                send_log(log)

                spark.stop()

        else:
            break
conn.close() #end of retrieving process
producer.close()

now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = now+": Rakuten connection closed. Starting join..."
print(msg)
log = format_log_message(msg = msg,
                        percentage = 70,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

#start consuming queue

stream_reviews = spark \
  .readStream \
  .format("kafka") \
  .option("kafka.bootstrap.servers", config["broker_configuration"][0]["url"]+":"+str(config["broker_configuration"][0]["port"])) \
  .option("subscribe", topic) \
  .option("includeHeaders", "true") \
  .option("startingOffsets", "earliest") \
  .load()
stream_reviews.selectExpr("CAST(value AS STRING)", "headers")

reviews_schema = make_review_schema(interesting_fields)

#this is the first dataframe, reviews_schema is all coded inside "value" column
reviews_df = stream_reviews.select(from_json(decode(col("value"),'utf-8'), reviews_schema).alias("reviews"))

#here the reviews_schema is unpacked and every field is now a column (as expected)
spark_reviews_DF = reviews_df.select("reviews.*")

spark_lookup_DF=spark.createDataFrame(lookup_df)

spark_reviews_DF = spark_reviews_DF.join(spark_lookup_DF, spark_reviews_DF.hotel_id == spark_lookup_DF.booking_id) \
                                   .select(prepare_spark_select_fields(interesting_fields))

"""
spark_reviews_DF = spark_reviews_DF.join(spark_lookup_DF, spark_reviews_DF.hotel_id == spark_lookup_DF.booking_id) \
                                   .select(
                                        spark_lookup_DF.facility_ID.alias("facility_id"),
                                        spark_reviews_DF.hotel_id.alias('booking_id'),
                                        spark_reviews_DF.title,
                                        spark_reviews_DF.pros,
                                        spark_reviews_DF.cons,
                                        spark_reviews_DF.date,
                                        spark_reviews_DF.average_score)
                                        """

# Start running the query that prints the running counts to the console
#spark_reviews_DF.printSchema()

print(output_dir)
#repartition(1) specifies in how many files spark saves the data, in this case we want just 1 file

query = spark_reviews_DF \
    .repartition(1) \
    .writeStream \
    .format("csv") \
    .option("path", output_dir+"/rakuten_2-04-2022") \
    .option("checkpointLocation", output_dir+"/rakuten_2-04-2022") \
    .option("header", True) \
    .outputMode("append") \
    .start()

#detector for the end of the queue
stop_stream_query(query,5)

for i,filename in enumerate(glob.glob(output_dir+"/rakuten_2-04-2022/part*.csv")):
    file = open(filename)
    reader = csv.reader(file)
    row_count = sum(1 for row in reader)
    print(row_count)
"""
try:
    rename_output_files(output_dir,make_output_file_name(config["outputs"][0][0]["config"]["entity"]))
    remove_output_files(output_dir)
except Exception as e:
    print(str(e))
    spark.stop()
"""
#detele topic

client = KafkaClient(bootstrap_servers=config["broker_configuration"][0]["url"]+":"+str(config["broker_configuration"][0]["port"]))
admin_client = KafkaAdminClient(bootstrap_servers=config["broker_configuration"][0]["url"]+":"+str(config["broker_configuration"][0]["port"]))
deletion = admin_client.delete_topics([topic])
try:
    future = client.cluster.request_update()
    client.poll(future=future)
except Exception as e:
    print(str(e))
finally:
    client.close()
    admin_client.close()

print("total extraction: "+str(total_extractions))

now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = now+": "+PIPELINE_LOGGER_MESSAGE_STATUS_END
print(msg)
log = format_log_message(msg = PIPELINE_LOGGER_MESSAGE_STATUS_END,
                        percentage = 100,
                        status = PIPELINE_LOGGER_STATUS_END,
                        ts = int(time.time()*1000))
send_log(log)

spark.stop()
