#questa funziona anche con spark submit
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType
##
import requests
import json
import os
from time import sleep, time

## APIFY EVENTS

ACTOR_RUN_CREATED = "ACTOR.RUN.CREATED"
ACTOR_RUN_SUCCEDEED = "ACTOR.RUN.SUCCEEDED"
ACTOR_RUN_FAILED = "ACTOR.RUN.SUCCEEDED"
ACTOR_BUILD_CREATED = "ACTOR.BUILD.CREATED"
ACTOR_BUILD_SUCCEDEED = "ACTOR.BUILD.SUCCEDEED"
ACTOR_BUILD_FAILED = "ACTOR.BUILD.FAILED"

# FF 
STATUS_RUNNING = "Running"
STATUS_STOPPED = "Stopped"

PIPELINE_LOGGER_MESSAGE_STATUS_END = "END PIPELINE"
PIPELINE_LOGGER_MESSAGE_STATUS_START = "START PIPELINE"
PIPELINE_LOGGER_STATUS_START = "START"
PIPELINE_LOGGER_STATUS_END = "END"
PIPELINE_LOGGER_STATUS_RUNNING = "RUNNING"
PIPELINE_LOGGER_STATUS_ERROR = "ERROR"
PIPELINE_LOGGER_STATUS_OK = "OK"

MSG_INSTANCE_STARTED = "Pipeline instance has started"
MSG_BROKER_CONNECTED = "Broker connection established"
MSG_BROKER_ERROR = "Broker connetion failed"
MSG_DATASOURCE_CONNECTED = "Data source is connected"
MSG_DATASOURCE_ERROR = "Data source connection failed"
MSG_INPROGRESS = "Data collection in progress..."
MSG_DATA_COLLECTION_COMPLETE = "Data collection complete. Processing has started..."
MSG_PROCESSING_COMPLETE = "Processing complete. Ingestion to target has started..."
MSG_INGESTION_COMPLETE = "Ingestion to target complete"
MSG_NUM_RECORDS_INGESTED = " records ingested"
MSG_INSTANCE_STOPPED = "Pipeline instance stopped"
MSG_CONFIG_ERROR = "Error fetching configuration"

           
def load_env():
    return os.environ.get('pyspooq_env_globals')

def env_to_json(conf_string: str) -> dict:
    from json import loads
    #retrieve settings from env (spark-submit body)
    try:
        if len(conf_string) > 0:
            conf = loads(conf_string.replace("'", "\""))
        else:
            raise Exception("empty conf string")
    except:
        return {}
    else:
        spooq_conf = {}
        for key in conf:
            spooq_conf[key] = conf.get(key, -1)

        return spooq_conf

def format_init_log_message(instance_id, pid, run_id):

    log = {
        "key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*",
        "spark_log": True,
        "log_data": {
            "run_id": run_id,
            "pid": pid,
            "instance_id": instance_id,
            "version": "0.1"
        }
    }
    return log

def format_log_message(msg, percentage, status, ts, job_info):

    log = {
        "key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*",
        "log_data": {
            "percentage": percentage,
            "version": "0.1",
            "status": status,
            "policies_id": "None",
            "message": msg,
            "instance_id": job_info.get("instance_id", 0),
            "job_configuration_id": job_info.get("job_conf_id", 0),
            "timestamp": ts,
            "pipeline_id": job_info.get("pipeline_id", 0)
        }
    }
    return log

def send_log(log, log_service_url):
    headers = {}
    headers["Content-Type"] = "application/json"
    json_string = json.dumps(log)

    try:
        response = requests.post(url=log_service_url, data=json_string, headers=headers, timeout=5)
        response.close()

        if response.status_code >= 400:
            return False
        else:
            return True
            
    except Exception as e:
        return False

def apify_webhook_emulator(run_id, webhook, event_type, ts):

    if event_type == ACTOR_BUILD_CREATED:
        percentage = 65
    elif event_type == ACTOR_RUN_SUCCEDEED:
        percentage = 90
    
    body = {
        "userId": "something",
        "createdAt": ts,
        "eventType": event_type,
        "eventData": [],
        "eventData": {
            "actorId": "something",
            "actorRunId": run_id
        },
        "customField": "phase_2",
        "customPercentage": percentage
    }

    send_log(body, webhook)
    return True

def phase_one(env_globals, pid):
    #log = format_init_log_message(816, pid, "asd342kam-")
    log = format_init_log_message(env_globals.get('instance_id', 0), pid, "asd342kam-")
    send_log(log, env_globals.get('logs_service_url', 'http://localhost:8000/fusilli/logger/save-log/'))

    sleep(10) #simulating apify request processing
    return True

def phase_two(env_globals, pid, run_id):
    # webhook = env_globals.get('logs_service_url', 'http://localhost:8000/fusilli/logger/save-log/')

    # those calls has to be simulated with postman manually
    # ts = int(time.time()*1000)
    # apify_webhook_emulator(run_id, webhook, ACTOR_BUILD_CREATED, ts)

    # sleep(5) #simulating apify processing

    # ts = int(time.time()*1000)
    # apify_webhook_emulator(run_id, webhook, ACTOR_RUN_SUCCEDEED, ts)

    #log = format_init_log_message(env_globals.get('instance_id', 0), pid, run_id)

    # the section below should not be implemented in a connector. It has to be as simple as possible

    # ts = int(time.time()*1000)
    # log = format_log_message(
    #                         "Reviews fetched from source", 
    #                         80,
    #                         STATUS_RUNNING,
    #                         ts,
    #                         env_globals)
    # send_log(log, env_globals.get('logs_service_url', 'http://localhost:8000/fusilli/logger/save-log/'))

    sleep(5) #simulating apify processing

    # ts = int(time.time()*1000)
    # log = format_log_message(
    #                         "Reviews ingested on Kafka", 
    #                         90,
    #                         STATUS_RUNNING,
    #                         ts,
    #                         env_globals)
    # send_log(log, env_globals.get('logs_service_url', 'http://localhost:8000/fusilli/logger/save-log/'))

    return True

class pyspooqGlobalEnvsPrinter:
    def __init__(self):
        self.udf = udf(pyspooqGlobalEnvsPrinter.run_connector, StringType())

    @staticmethod
    def run_connector():
        env_globals_str = load_env()
        env_globals = env_to_json(env_globals_str)

        #retrieve and send the job's pid to backend
        pid = os.getpid()

        phase_no = 1 if env_globals.get("run_id", 0) == 0 else 2

        if phase_no == 1:
            phase_one(env_globals, pid)
        else:
            phase_two(env_globals, pid, env_globals.get("run_id", 0))
        

        return f"connector phase {phase_no} done"
