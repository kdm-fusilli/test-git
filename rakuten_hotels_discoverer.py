import http.client
import json
import pandas as pd
from pyspark.sql import SparkSession
import ssl
import datetime as dt
import time
import requests
import os
import sys
import signal

STATUS_RUNNING = "Running"
STATUS_STOPPED = "Stopped"
PIPELINE_LOGGER_MESSAGE_STATUS_END = "END PIPELINE"
PIPELINE_LOGGER_MESSAGE_STATUS_START = "START PIPELINE"
PIPELINE_LOGGER_STATUS_START = "START"
PIPELINE_LOGGER_STATUS_END = "END"
PIPELINE_LOGGER_STATUS_RUNNING = "RUNNING"
PIPELINE_LOGGER_STATUS_ERROR = "ERROR"
PIPELINE_LOGGER_STATUS_OK = "OK"

MSG_INSTANCE_STARTED = "Pipeline instance has started"
MSG_BROKER_CONNECTED = "Broker connection established"
MSG_BROKER_ERROR = "Broker connetion failed"
MSG_DATASOURCE_CONNECTED = "Data source is connected"
MSG_DATASOURCE_ERROR = "Data source connection failed"
MSG_INPROGRESS = "Data collection in progress..."
MSG_DATA_COLLECTION_COMPLETE = "Data collection complete. Processing has started..."
MSG_PROCESSING_COMPLETE = "Processing complete. Ingestion to target has started..."
MSG_INGESTION_COMPLETE = "Ingestion to target complete"
MSG_NUM_RECORDS_INGESTED = " records ingested"
MSG_INSTANCE_STOPPED = "Pipeline instance stopped"
MSG_CONFIG_ERROR = "Error fetching configuration"

MSG_LOG_HOSTNAME = "dev.fyrefuse.io"
MSG_LOG_PORT = "443"

DEST_ID = 186 #Saudi Arabia
DEST_TYPE = "country"
days_span = 7

increment_percentage = int(1/days_span*100) #status increment step

# to make conversion from JS to Py date string format
js_to_python_dt = {
    "YYYY": "%Y",
    "YY": "%y",
    "MM": "%m",
    "MMM": "%b",
    "MMMM": "%B",
    "DD": "%d"
}

def custom_signal_handler(signal, frame):
    global stopped
    try:
        conn.close()
        stopped = True
    except Exception as e:
        print(str(e))
    finally:
        now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        msg = MSG_INSTANCE_STOPPED
        log = format_log_message(msg = msg,
                                percentage = actual_percentage,
                                status = STATUS_STOPPED,
                                ts = int(time.time()*1000))
        send_log(log)
        print(now+": "+msg)
        spark.stop()
        sys.exit(0)

def term_exc_listener():
    signal.signal(signal.SIGTERM, custom_signal_handler)

def load_config_from_env():
    try:
        inputs = json.loads(spark.conf.get("spark.executerENV.inputs").replace("'", "\""))
        outputs = json.loads(spark.conf.get("spark.executerENV.outputs").replace("'", "\""))
        kafka_config = json.loads(spark.conf.get("spark.executerENV.kafka_config").replace("'", "\""))
        repo_params = json.loads(spark.conf.get("spark.executerENV.repo_credentials").replace("'", "\""))
        metadata = json.loads(spark.conf.get("spark.executerENV.metadata").replace("'", "\""))
        jobs = json.loads(spark.conf.get("spark.executerENV.jobs").replace("'", "\""))

        config = inputs
        #concat all the dict as one
        config.update(outputs)
        config.update(kafka_config)
        config.update(repo_params)
        config.update(metadata)
        config.update(jobs)

    except:
        config = False
    finally:
        return config

def format_kill_log_message(pid):
    try:
        instance_id = config["metadata"]["instance_id"][0] #perchè arriva un array?
    except:
        instance_id = config["metadata"]["instance_id"]

    log = {
        "key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*",
        "spark_log": True,
        "log_data": {
            "pid": pid,
            "instance_id": instance_id,
            "version": "0.1"
        }
    }
    return log

#percentage: between 0 - 100
#status could be: RUNNING, COMPLETED, SUCCEEDED, FAILED
def format_log_message(msg,percentage,status,ts):
    try:
        instance_id = config["metadata"]["instance_id"][0] #perchè arriva un array?
    except:
        instance_id = config["metadata"]["instance_id"]
    
    job_conf_id = config["jobs"][0]
    pipeline_id = config["metadata"]["pipeline_id"]
    log = {
        "key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*",
        "log_data": {
            "percentage": percentage,
            "version": "0.1",
            "status": status,
            "policies_id": "None",
            "message": msg,
            "instance_id": instance_id,
            "job_configuration_id": job_conf_id,
            "timestamp": ts,
            "pipeline_id": pipeline_id
        }
    }
    return log

def send_log(log):
    #hostname = "localhost:8000"
    hostname = "{host}:{port}".format(host=MSG_LOG_HOSTNAME, port=MSG_LOG_PORT)
    print("logging to hostname: {}".format(hostname))
    headers = {}
    headers["Content-Type"] = "application/json"
    json_string = json.dumps(log)

    try:
        response = requests.post(url='http://'+hostname+'/fusilli/logger/save-log/',
                                data=json_string, headers=headers, timeout=5)
        response.close()
        if response.status_code >= 400:
            return False
        else:
            return True
            
    except Exception as e:
        print(str(e))
        return False

def daterange(start_date, delta): #returns an iterable of the desired dates range
    for n in range(delta.days):
        yield start_date + dt.timedelta(n)

def wait_and_reopen_connection_backoff(conn,host,actual_perc):
    i = 0
    while True:
        if stopped:
            spark.stop()
            sys.exit(0)
        try:
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = "Reopening connection to the data source. Waiting {} seconds...".format(str(2**i))
            print(now+": "+msg)
            log = format_log_message(msg = msg,
                            percentage = actual_perc,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
            send_log(log)
            time.sleep(2**i) #backoff waiting
            conn = http.client.HTTPSConnection(host)
            return conn
        except:
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = MSG_DATASOURCE_ERROR+" Retrying..."
            print(now+": "+msg)
            log = format_log_message(msg = msg,
                            percentage = actual_perc,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
            send_log(log)
            i += 1

def get_hotel_info(hotel,fields):

    hotel_final = []
    #expected fields ["hotel_id","hotel_name","latitude","longitude","city"]
    for field in fields:
        field = field["attribute"]
        try:
            field_content = hotel[field]
        except:
            field_content = ""
        
        if type(field_content) == str:
            field_content = field_content.replace("\n"," ").replace("\r"," ").strip() #cleaning
        
        hotel_final.append(field_content) # building a new clean array

    return hotel_final

def set_page_limit(days_span,time_limit):
    #500 pages takes about 10 mins
    time_limit_min = time_limit*60
    daily_time_limit = time_limit_min/days_span
    # 500:10 = x: daily_time_limit
    return int(daily_time_limit*500//10)

### ENTRYPOINT ###
spark = SparkSession\
        .builder\
        .appName("Fyrefuse_Job-rakuten-hotels-discovery")\
        .getOrCreate()

spark.sparkContext.setLogLevel("ERROR")
stopped = False
#register the signal handler
term_exc_listener()

pid = os.getpid()

"""
config = load_config_from_file("./config.json")
if not config:
    print("Error opening config file")
    sys.exit(0)
"""

actual_percentage = 10
config = load_config_from_env()
if not config:
    msg = MSG_CONFIG_ERROR
    print(msg)
    log = format_log_message(msg = msg,
                            percentage = actual_percentage,
                            status = PIPELINE_LOGGER_STATUS_ERROR,
                            ts = int(time.time()*1000))
    send_log(log)
    spark.stop()
    sys.exit(0)

#send the pid to backend
log = format_kill_log_message(pid)
send_log(log)

now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = MSG_INSTANCE_STARTED
print(now+": "+msg)
log = format_log_message(msg = msg,
                         percentage = actual_percentage,
                         status = STATUS_RUNNING,
                         ts = int(time.time()*1000))
send_log(log)

#getting parameters from config
interesting_fields = config["inputs"][0]
rakuten_host = config["repositories"][0][0]["host"] # "booking-com.p.rapidapi.com" 
rakuten_token = config["repositories"][0][0]["token"]
output_dir = config["outputs"][0][0]["config"]["repository"]["schema"] #absolute path
output_dir = output_dir if output_dir[-1] == "/" else output_dir+"/" #add / to the end
facility_file = output_dir+"Match BookingID with FacilityID v.0.2.xlsx" #this file is 1 level back
output_dir = output_dir+"rakuten_"+str(dt.datetime.now().strftime("%Y-%m-%d"))+"/" #new folder every day
try:
    instance_id = config["metadata"]["instance_id"][0] #sometimes it returns an array instead of an object...
except:
    instance_id = config["metadata"]["instance_id"]

#The job is supposed to run for 8 hours
page_upper_limit = set_page_limit(days_span,8) #8 hours

#loading lookup table
try:
    lookup_df = pd.read_excel(facility_file, usecols=["booking_id","facility_ID"]) 
    lookup_df = lookup_df.dropna() #to delete with ultimate facility_file (?)
except Exception as e:
    msg = e
    print(now+": "+msg)
    log = format_log_message(msg = msg,
                            percentage = actual_percentage,
                            status = PIPELINE_LOGGER_STATUS_ERROR,
                            ts = int(time.time()*1000))
    send_log(log)
    spark.stop()
    sys.exit(0)

#connecting to rakuten
rakuten_request_headers = {
    'x-rapidapi-key': rakuten_token,
    'x-rapidapi-host': rakuten_host
}
actual_percentage += 5

try:
    conn = http.client.HTTPSConnection(rakuten_host)
except:
    try:
        conn = http.client.HTTPSConnection(rakuten_host,context = ssl._create_unverified_context())
    except:
        msg = MSG_DATASOURCE_ERROR
        print(now+": "+msg)
        log = format_log_message(msg = msg,
                                percentage = actual_percentage,
                                status = PIPELINE_LOGGER_STATUS_ERROR,
                                ts = int(time.time()*1000))
        send_log(log)
        spark.stop()
        sys.exit(0)

    """
    msg = now+":Unverified context created."
    print(msg)
    log = format_log_message(msg = msg,
                            percentage = 15,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
    send_log(log)
    """

now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = MSG_DATASOURCE_CONNECTED
print(now+": "+msg)
log = format_log_message(msg = msg,
                        percentage = actual_percentage,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

now = dt.datetime.now()
delta = dt.timedelta(days=days_span)
hotels = []
status_idx = 0 #used to calculate the job progression
total_extractions = 0 #used to count the extracted hotels

for single_date in daterange(now,delta):
    checkin = single_date.strftime("%Y-%m-%d")
    checkout = (single_date+dt.timedelta(days=1)).strftime("%Y-%m-%d")
    actual_percentage += int(increment_percentage*0.7) #0.70 is the 'weight' of this section, 70% of the entire job
    log = format_log_message(msg = MSG_INPROGRESS,
                            percentage = actual_percentage,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
    send_log(log)

    for page in range(page_upper_limit): #retrieving all the hotels

        query = """/v1/hotels/search?order_by=popularity&dest_id=%s&dest_type=%s&locale=en-gb&room_number=1&units=metric&adults_number=1&filter_by_currency=AED&page_number=%s&checkin_date=%s&checkout_date=%s"""%(str(DEST_ID), DEST_TYPE, page, checkin, checkout)

        time.sleep(0.15) #0.15s every call

        try:
            conn.request("GET", query, headers=rakuten_request_headers)
            res = conn.getresponse()
        except Exception as e:
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = MSG_DATASOURCE_ERROR
            print(now+": "+msg)
            log = format_log_message(msg = msg,
                                    percentage = actual_percentage,
                                    status = STATUS_RUNNING,
                                    ts = int(time.time()*1000))
            send_log(log)
            conn.close()

            conn = wait_and_reopen_connection_backoff(conn,rakuten_host,actual_percentage) #it waits until connection is reopened
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = MSG_DATASOURCE_CONNECTED
            print(now+": "+msg)
            log = format_log_message(msg = msg,
                                    percentage = actual_percentage,
                                    status = STATUS_RUNNING,
                                    ts = int(time.time()*1000))
            send_log(log)
            page -= 1
            continue
        
        if res.getcode() != 200: #call failed, skipping page
            continue
        
        try:
            data = res.read()
            json_data = json.loads(data)
        except:
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = "Error reading data from data source. Retrying..."
            print(now+": "+msg)
            continue

        n_hotels = len(json_data["result"])

        #print("cin: {} cout: {} page: {} total extractions: {}".format(checkin,checkout,page,total_extractions))
        if(n_hotels > 0):
            total_extractions += n_hotels
            for i in range(n_hotels):
                hotel_info = get_hotel_info(json_data["result"][i],interesting_fields)
                hotels.append(hotel_info)
            
        else:
            break

conn.close()
fields = [] #building dynamic column list
for field in interesting_fields:
    fields.append(field["attribute"])

extracted_hotels_df = pd.DataFrame(hotels, columns=fields)
#print("len df prima drop: {}".format(len(extracted_hotels_df.index)))

actual_percentage += 5 #90% here
now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = MSG_DATA_COLLECTION_COMPLETE
print(now+": "+msg)
log = format_log_message(msg = msg,
                        percentage = actual_percentage,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

#The endpoint returns duplicated hotels
extracted_hotels_df.drop_duplicates(subset=['hotel_id'],inplace=True)
#print("len df dopo drop: {}".format(len(extracted_hotels_df.index)))

#drop existing hotels
cond = extracted_hotels_df['hotel_id'].isin(lookup_df['booking_id'])
discovered_hotels_df = extracted_hotels_df.drop(extracted_hotels_df[cond].index, inplace = False)

#saving results (if any) to filesystem
if len(discovered_hotels_df.index) > 0:

    actual_percentage += 4
    now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    msg = MSG_PROCESSING_COMPLETE
    print(now+": "+msg)
    log = format_log_message(msg = msg,
                            percentage = actual_percentage,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
    send_log(log)

    #saving
    try:
        os.makedirs(output_dir, exist_ok=True)
        filename = "discovered_hotels_{}.csv".format(str(dt.datetime.now().strftime("%Y-%m-%d")))
        discovered_hotels_df.to_csv(output_dir+filename,index=False, encoding='utf-8-sig')
    except Exception as e:
        now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        msg = str(e)
        print(now+": "+msg)
        log = format_log_message(msg = msg,
                                percentage = actual_percentage,
                                status = PIPELINE_LOGGER_STATUS_ERROR,
                                ts = int(time.time()*1000))
        send_log(log)
        spark.stop()
        sys.exit(0)

    actual_percentage += 4 #98%
    now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    msg = MSG_INGESTION_COMPLETE
    print(now+": "+msg)
    log = format_log_message(msg = msg,
                            percentage = actual_percentage,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
    send_log(log)


msg = str(len(discovered_hotels_df.index))+MSG_NUM_RECORDS_INGESTED
print(now+": "+msg)
log = format_log_message(msg = msg,
                        percentage = actual_percentage,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

msg = now+": "+PIPELINE_LOGGER_MESSAGE_STATUS_END
print(msg)
log = format_log_message(msg = PIPELINE_LOGGER_MESSAGE_STATUS_END,
                        percentage = 100,
                        status = PIPELINE_LOGGER_STATUS_END,
                        ts = int(time.time()*1000))
send_log(log)

spark.stop()
sys.exit(0)
