from pyspark.sql.functions import udf
from pyspark.sql.types import IntegerType


class sumUDF():
    def __init__(self):
        print("sumUDF init")
        self.udf = udf(sumUDF.sum, IntegerType())

    @staticmethod
    def sum(n1, n2):

        result = n1 + n2
        return result
