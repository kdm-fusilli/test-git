#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import sys
from random import random
from operator import add
import json
import requests
import time
from pyspark.sql import SparkSession


if __name__ == "__main__":
    """
        Usage: pi [partitions]
    """
    spark = SparkSession\
        .builder\
        .appName("PythonPi")\
        .getOrCreate()

    partitions = int(sys.argv[1]) if len(sys.argv) > 1 else 2
    n = 100000 * partitions

    def f(_):
        x = random() * 2 - 1
        y = random() * 2 - 1
        return 1 if x ** 2 + y ** 2 <= 1 else 0

    count = spark.sparkContext.parallelize(range(1, n + 1), partitions).map(f).reduce(add)

    def format_log_message(msg,percentage,status,ts):
        try:
            instance_id = config["metadata"]["instance_id"][0] #perchè arriva un array?
        except:
            instance_id = config["metadata"]["instance_id"]
        pipeline_id = config["metadata"]["pipeline_id"]

        log = {
            "key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*",
            "log_data": {
                "percentage": percentage,
                "version": "0.1",
                "status": status,
                "policies_id": "b'3LJLB\\xfd\\xb7\\x9d~\\xbc>s\\xb5\\x17\\xe6\\xf8'",
                "message": msg,
                "instance_id": instance_id,
                "job_configuration_id": 36,
                "timestamp": ts,
                "pipeline_id": pipeline_id
            }
        }
        return log

    def send_log(log):
        #hostname = "localhost:8000"
        hostname = "fusilli-dev-2:8001"
        headers = {}
        headers["Content-Type"] = "application/json"
        json_string = json.dumps(log)

        response = requests.post(url='http://'+hostname+'/fusilli/logger/save-log/',
                                data=json_string, headers=headers)
        if response.status_code >= 400:
            return False
        else:
            return True

    try:
        print("ENV VAR = "+ spark.conf.get("spark.executerENV.inputs"))
        print("ENV VAR = "+ spark.conf.get("spark.executerENV.outputs"))
        print("ENV VAR = "+ spark.conf.get("spark.executerENV.kafka_config"))
        print("ENV VAR = "+ spark.conf.get("spark.executerENV.repo_credentials"))
        print("ENV VAR = "+ spark.conf.get("spark.executerENV.metadata"))

        inputs = json.loads(spark.conf.get("spark.executerENV.inputs").replace("'", "\""))
        outputs = json.loads(spark.conf.get("spark.executerENV.outputs").replace("'", "\""))
        kafka_config = json.loads(spark.conf.get("spark.executerENV.kafka_config").replace("'", "\""))
        repo = json.loads(spark.conf.get("spark.executerENV.repo_credentials").replace("'", "\""))
        meta = json.loads(spark.conf.get("spark.executerENV.metadata").replace("'", "\""))
        config = inputs

        config.update(outputs)
        config.update(kafka_config)
        config.update(repo)
        config.update(meta)

        json_formatted_str = json.dumps(config, indent=4)
        print(json_formatted_str)

        with open("./json_verde.json", "w") as outfile:
            json.dump(config, outfile)
        
        
        log = format_log_message(msg = "loggata finale",
                            percentage = 100,
                            status = "Succeeded",
                            ts = int(time.time()*1000))
        send_log(log)

    except Exception as e:
        print(e)

    print("Pi is roughly %f" % (4.0 * count / n))

    spark.stop()
