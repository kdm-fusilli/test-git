#questa funziona anche con spark submit
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

def load_env():
    import os
    return os.environ.get('pyspooq_env_globals')

def load_spooq_conf(conf_string: str) -> dict:
    from json import loads
    #retrieve settings from env (spark-submit body)
    try:
        if len(conf_string) > 0:
            conf = loads(conf_string.replace("'", "\""))
        else:
            raise Exception("empty conf string")
    except:
        return {}
    else:
        spooq_conf = {}
        for key in conf:
            spooq_conf[key] = conf.get(key, -1)

        return spooq_conf

class pyspooqGlobalEnvsPrinter:
    def __init__(self):
        self.udf = udf(pyspooqGlobalEnvsPrinter.show_globals, StringType())

    @staticmethod
    def show_globals():
        env_globals_str = load_env()
        env_globals = load_spooq_conf(env_globals_str)
        return f"pyspooq_env_globals: {env_globals_str}"
        #return f"Hello, {name} ; ENV: {env_globals.get('env1')}"
