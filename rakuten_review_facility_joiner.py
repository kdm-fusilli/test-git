import http.client
import pandas as pd
import json
import sys
from time import sleep
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *
from kafka import KafkaClient, KafkaProducer, TopicPartition, KafkaAdminClient
import ssl
import glob
import os
import re
import datetime as dt
import time
import shutil
import requests
import time
import signal

STATUS_RUNNING = "Running"
STATUS_STOPPED = "Stopped"
PIPELINE_LOGGER_MESSAGE_STATUS_END = "END PIPELINE"
PIPELINE_LOGGER_MESSAGE_STATUS_START = "START PIPELINE"
PIPELINE_LOGGER_STATUS_START = "START"
PIPELINE_LOGGER_STATUS_END = "END"
PIPELINE_LOGGER_STATUS_RUNNING = "RUNNING"
PIPELINE_LOGGER_STATUS_ERROR = "ERROR"
PIPELINE_LOGGER_STATUS_OK = "OK"

MSG_INSTANCE_STARTED = "Pipeline instance has started"
MSG_BROKER_CONNECTED = "Broker connection established"
MSG_BROKER_ERROR = "Broker connetion failed"
MSG_DATASOURCE_CONNECTED = "Data source is connected"
MSG_DATASOURCE_ERROR = "Data source connection failed"
MSG_INPROGRESS = "Data collection in progress..."
MSG_DATA_COLLECTION_COMPLETE = "Data collection complete. Processing has started..."
MSG_PROCESSING_COMPLETE = "Processing complete. Ingestion to target has started..."
MSG_INGESTION_COMPLETE = "Ingestion to target complete"
MSG_NUM_RECORDS_INGESTED = " records ingested"
MSG_INSTANCE_STOPPED = "Pipeline instance stopped"
MSG_CONFIG_ERROR = "Error fetching configuration"

# to make conversion from JS to Py date string format
js_to_python_dt = {
    "YYYY": "%Y",
    "YY": "%y",
    "MM": "%m",
    "MMM": "%b",
    "MMMM": "%B",
    "DD": "%d"
}

def custom_signal_handler(signal, frame):
    global stopped
    try:
        conn.close()
        producer.close()
        delete_topic()
        stopped = True
    except Exception as e:
        print(str(e))
    finally:
        now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        msg = MSG_INSTANCE_STOPPED
        log = format_log_message(msg = msg,
                                percentage = actual_percentage,
                                status = STATUS_STOPPED,
                                ts = int(time.time()*1000))
        send_log(log)
        print(now+": "+msg)
        spark.stop()
        sys.exit(0)

def term_exc_listener():
    signal.signal(signal.SIGTERM, custom_signal_handler)

def format_kill_log_message(pid):
    try:
        instance_id = config["metadata"]["instance_id"][0] #perchè arriva un array?
    except:
        instance_id = config["metadata"]["instance_id"]

    log = {
        "key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*",
        "spark_log": True,
        "log_data": {
            "pid": pid,
            "instance_id": instance_id,
            "version": "0.1"
        }
    }
    return log

#percentage: between 0 - 100
#status could be: RUNNING, COMPLETED, SUCCEEDED, FAILED
def format_log_message(msg,percentage,status,ts):
    try:
        instance_id = config["metadata"]["instance_id"][0] #perchè arriva un array?
    except:
        instance_id = config["metadata"]["instance_id"]
    
    job_conf_id = config["jobs"][0]
    pipeline_id = config["metadata"]["pipeline_id"]
    log = {
        "key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*",
        "log_data": {
            "percentage": percentage,
            "version": "0.1",
            "status": status,
            "policies_id": "None",
            "message": msg,
            "instance_id": instance_id,
            "job_configuration_id": job_conf_id,
            "timestamp": ts,
            "pipeline_id": pipeline_id
        }
    }
    return log

def send_log(log):
    #hostname = "localhost:8000"
    hostname = "fusilli-dev-2:8001"
    headers = {}
    headers["Content-Type"] = "application/json"
    json_string = json.dumps(log)

    try:
        response = requests.post(url='http://'+hostname+'/fusilli/logger/save-log/',
                                data=json_string, headers=headers, timeout=5)
        response.close()
        if response.status_code >= 400:
            return False
        else:
            return True
            
    except Exception as e:
        print(str(e))
        return False

def load_config_from_env():
    try:
        inputs = json.loads(spark.conf.get("spark.executerENV.inputs").replace("'", "\""))
        outputs = json.loads(spark.conf.get("spark.executerENV.outputs").replace("'", "\""))
        kafka_config = json.loads(spark.conf.get("spark.executerENV.kafka_config").replace("'", "\""))
        repo_params = json.loads(spark.conf.get("spark.executerENV.repo_credentials").replace("'", "\""))
        metadata = json.loads(spark.conf.get("spark.executerENV.metadata").replace("'", "\""))
        jobs = json.loads(spark.conf.get("spark.executerENV.jobs").replace("'", "\""))

        config = inputs
        #concat all the dict as one
        config.update(outputs)
        config.update(kafka_config)
        config.update(repo_params)
        config.update(metadata)
        config.update(jobs)

    except:
        config = False
    finally:
        return config

def load_config_from_file(filename):
    try:
        with open(filename) as f:
            conf = json.load(f)
    except:
        conf = False
    finally:
        return conf

def wait_and_reopen_connection_backoff(conn,host,actual_perc):
    i = 0
    while True:
        if stopped:
            spark.stop()
            sys.exit(0)
        try:
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = "Reopening connection to the data source. Waiting {} seconds...".format(str(2**i))
            print(now+": "+msg)
            log = format_log_message(msg = msg,
                            percentage = actual_perc,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
            send_log(log)
            sleep(2**i) #backoff waiting
            conn = http.client.HTTPSConnection(host)
            return conn
        except:
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = MSG_DATASOURCE_ERROR+" Retrying..."
            print(now+": "+msg)
            log = format_log_message(msg = msg,
                            percentage = actual_perc,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
            send_log(log)
            i += 1

def format_review(full_json_response,fields):
    review = {}
    #expected fields ["pros","cons","date"]
    for field in fields:
        field = field["attribute"]
        try:
            field_content = full_json_response[field]
        except:
            field_content = ""

        if type(field_content) == str:
            field_content = field_content.replace("\n"," ").replace("\r"," ").strip() #cleaning string

        review[field] = field_content # building a new clean dict

    return review

def make_review_schema(fields):
    
    schema = StructType()
    for field in fields:
        field = field["attribute"]
        schema.add(field,StringType())

    return schema

def prepare_spark_select_fields(fields):
    select = ['facility_id']

    for field in fields:
        field = field["attribute"]
        select.append(field)

    return select

def rename_output_files(dir,output_prefix):
    for i,file in enumerate(glob.glob(dir+"part*.csv")):
        os.rename(file,dir+output_prefix+str(i)+".csv")

def remove_output_files(dir):

    os.remove(dir+"metadata")
    shutil.rmtree(dir+"sources")
    shutil.rmtree(dir+"offsets")
    shutil.rmtree(dir+"commits")
    shutil.rmtree(dir+"_spark_metadata")
    
    for file in glob.glob(dir+".*"):
        os.remove(file)
    for file in glob.glob(dir+"_*"):
        os.remove(file)    
    for file in glob.glob(dir+"_*.crc"):
        os.remove(file)

def make_output_file_name(format):

    part_start_idx = format.find("_part")
    dt_format = format.split("_part")[0]

    dt_py_format = ""
    for sym in dt_format.split("-"):
        dt_py_format += js_to_python_dt[sym]+"-"
    dt_py_format = dt_py_format[:-1]

    string_dt = str(dt.datetime.now().strftime(dt_py_format))

    fname = string_dt+format[part_start_idx:part_start_idx+5]
    return fname

def stop_stream_query(query, wait_time):
    """Stop a running streaming query"""
    while query.isActive:
        time.sleep(3)
        msg = query.status['message']
        data_avail = query.status['isDataAvailable']
        trigger_active = query.status['isTriggerActive']
        if not data_avail and not trigger_active and msg != "Initializing sources":
            print('Stopping query...')
            query.stop()

    # Okay wait for the stop to happen
    print('Awaiting termination...')
    query.awaitTermination(wait_time)

def delete_topic():
    client = KafkaClient(bootstrap_servers=broker_url+":"+broker_port)
    admin_client = KafkaAdminClient(bootstrap_servers=broker_url+":"+broker_port)
    deletion = admin_client.delete_topics([topic])
    try:
        future = client.cluster.request_update()
        client.poll(future=future)
    except Exception as e:
        print(str(e))
    finally:
        client.close()
        admin_client.close()

# ENTRYPOINT #
spark = SparkSession\
        .builder\
        .appName("Fyrefuse_Job-booking_facility_joiner")\
        .getOrCreate()

spark.sparkContext.setLogLevel("ERROR")
stopped = False

#register the signal handler
term_exc_listener()
pid = os.getpid()

"""
config = load_config_from_file("./config.json")
if not config:
    print("Error opening config file")
    spark.stop()
"""

actual_percentage = 10
config = load_config_from_env()
if not config:
    msg = MSG_CONFIG_ERROR
    print(msg)
    log = format_log_message(msg = msg,
                            percentage = actual_percentage,
                            status = PIPELINE_LOGGER_STATUS_ERROR,
                            ts = int(time.time()*1000))
    send_log(log)
    spark.stop()
    sys.exit(0)

#send the pid to backend
log = format_kill_log_message(pid)
send_log(log)

now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = MSG_INSTANCE_STARTED
print(now+": "+msg)
log = format_log_message(msg = msg,
                         percentage = actual_percentage,
                         status = STATUS_RUNNING,
                         ts = int(time.time()*1000))
send_log(log)

#getting parameters from config
broker_url = config["broker_configuration"][0]["url"]
broker_port = str(config["broker_configuration"][0]["port"])
interesting_fields = config["inputs"][0]
rakuten_host = config["repositories"][0][0]["host"] # "booking-com.p.rapidapi.com" 
rakuten_token = config["repositories"][0][0]["token"]
page_upper_limit = 100000
output_dir = config["outputs"][0][0]["config"]["repository"]["schema"] #absolute path
output_dir = output_dir if output_dir[-1] == "/" else output_dir+"/" #add / to the end
facility_file = output_dir+"Match BookingID with FacilityID v.0.2.xlsx" #this file is 1 level back
output_dir = output_dir+"rakuten_"+str(dt.datetime.now().strftime("%Y-%m-%d"))+"/" #new folder every day

try:
    instance_id = config["metadata"]["instance_id"][0] #sometimes it returns an array instead of an object...
except:
    instance_id = config["metadata"]["instance_id"]


match = re.search("(YY)*-*(M)*-+DD_part.*",config["outputs"][0][0]["config"]["entity"]) #check output format
if not match:
    msg = "Output filename not recognised..."
    print(now+": "+msg)
    log = format_log_message(msg = msg,
                            percentage = actual_percentage,
                            status = PIPELINE_LOGGER_STATUS_ERROR,
                            ts = int(time.time()*1000))
    send_log(log)
    spark.stop()
    sys.exit(0)

#loading lookup table
try:
    lookup_df = pd.read_excel(facility_file, usecols=["booking_id","facility_ID"]) 
    lookup_df = lookup_df.dropna() #to delete with ultimate facility_file
except Exception as e:
    msg = str(e)
    print(now+": "+msg)
    log = format_log_message(msg = msg,
                            percentage = actual_percentage,
                            status = PIPELINE_LOGGER_STATUS_ERROR,
                            ts = int(time.time()*1000))
    send_log(log)
    spark.stop()
    sys.exit(0)

hotels_number = len(lookup_df.index)
increment_percentage = hotels_number//10 #10% increment step

#initializing kafka producer
# TODO: implement authentication
actual_percentage += 5
try:
    producer = KafkaProducer(bootstrap_servers=[broker_url+":"+broker_port],
                            value_serializer=lambda x: 
                            json.dumps(x).encode('utf-8'))
except:
    msg = MSG_BROKER_ERROR
    print(now+": "+msg)
    log = format_log_message(msg = msg,
                            percentage = actual_percentage,
                            status = PIPELINE_LOGGER_STATUS_ERROR,
                            ts = int(time.time()*1000))
    send_log(log)
    spark.stop()
    sys.exit(0)

now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = MSG_BROKER_CONNECTED
print(now+": "+msg)
log = format_log_message(msg = msg,
                        percentage = actual_percentage,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

#pipeline_id + instance_id are enough for uniqueness
topic = "fusilli_queue-"+str(config["metadata"]["pipeline_id"])+"-"+str(instance_id)

#connecting to rakuten
rakuten_request_headers = {
    'x-rapidapi-key': rakuten_token,
    'x-rapidapi-host': rakuten_host
}

try:
    conn = http.client.HTTPSConnection(rakuten_host)
except:
    try:
        conn = http.client.HTTPSConnection(rakuten_host,context = ssl._create_unverified_context())
    except:
        msg = MSG_DATASOURCE_ERROR
        print(now+": "+msg)
        log = format_log_message(msg = msg,
                                percentage = actual_percentage,
                                status = PIPELINE_LOGGER_STATUS_ERROR,
                                ts = int(time.time()*1000))
        send_log(log)
        spark.stop()
        sys.exit(0)

    """
    msg = now+":Unverified context created."
    print(msg)
    log = format_log_message(msg = msg,
                            percentage = 15,
                            status = STATUS_RUNNING,
                            ts = int(time.time()*1000))
    send_log(log)
    """

now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = MSG_DATASOURCE_CONNECTED
print(now+": "+msg)
log = format_log_message(msg = msg,
                        percentage = actual_percentage,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

#retrieving all the reviews for all the matched hotels and
#saving them in the kafka queue
status_idx = 0 #used to calculate the job progression
total_extractions = 0 #used to count the reviews extracted
for idx,row in lookup_df.iterrows():
    status_idx += 1
    hotel_id = row["booking_id"]

    if status_idx % increment_percentage == 0: #increment % status
        #actual_percentage += int(status_idx//increment_percentage*10*0.70) #0.70 is the 'weight' of this section, 70% of the entire job
        actual_percentage += 7
        log = format_log_message(msg = MSG_INPROGRESS,
                                percentage = actual_percentage,
                                status = STATUS_RUNNING,
                                ts = int(time.time()*1000))
        send_log(log)

    for page in range(page_upper_limit): #retrieving all the results

        query = "/v1/hotels/reviews?locale=en-gb&sort_type=SORT_RECENT_DESC&hotel_id=%s&page_number=%s"%(hotel_id,page)
        sleep(0.15) #0.15s every call

        try:
            conn.request("GET", query, headers=rakuten_request_headers)
            res = conn.getresponse()
        except Exception as e:
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = "Connection lost with data source..."
            print(now+": "+msg)
            log = format_log_message(msg = msg,
                                    percentage = actual_percentage,
                                    status = STATUS_RUNNING,
                                    ts = int(time.time()*1000))
            send_log(log)
            conn.close()

            conn = wait_and_reopen_connection_backoff(conn,rakuten_host,actual_percentage) #it waits until connection is reopened
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = MSG_DATASOURCE_CONNECTED
            print(now+": "+msg)
            log = format_log_message(msg = msg,
                                    percentage = actual_percentage,
                                    status = STATUS_RUNNING,
                                    ts = int(time.time()*1000))
            send_log(log)
            page -= 1
            continue

        if res.getcode() != 200: #call failed, skipping page
            continue
        
        try:
            data = res.read()
            json_data = json.loads(data)
        except:
            now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            msg = "Error reading data from data source. Retrying..."
            print(now+": "+msg)
            """
            log = format_log_message(msg = msg,
                                    percentage = actual_percentage,
                                    status = STATUS_RUNNING,
                                    ts = int(time.time()*1000))
            send_log(log)
            """
            continue

        n_hotels = len(json_data["result"])

        if(n_hotels > 0):
            total_extractions += n_hotels
            for i in range(n_hotels):

                review = format_review(json_data["result"][i],interesting_fields)

                try:
                    producer.send(topic, value=review)
                except Exception as e:
                    now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                    msg = now+": "+str(e)
                    print(msg)
                    log = format_log_message(msg = msg,
                                    percentage = actual_percentage,
                                    status = STATUS_RUNNING,
                                    ts = int(time.time()*1000))
                    send_log(log)
        else:
            break
conn.close() #end of retrieving process
producer.close()

actual_percentage += 5 #90% here
now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = MSG_DATA_COLLECTION_COMPLETE
print(now+": "+msg)
log = format_log_message(msg = msg,
                        percentage = actual_percentage,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

#start consuming queue

stream_reviews = spark \
  .readStream \
  .format("kafka") \
  .option("kafka.bootstrap.servers", broker_url+":"+broker_port) \
  .option("subscribe", topic) \
  .option("includeHeaders", "true") \
  .option("startingOffsets", "earliest") \
  .load()
stream_reviews.selectExpr("CAST(value AS STRING)", "headers")

reviews_schema = make_review_schema(interesting_fields)

#this is the first dataframe, reviews_schema is all coded inside "value" column
reviews_df = stream_reviews.select(from_json(decode(col("value"),'utf-8'), reviews_schema).alias("reviews"))

#here the reviews_schema is unpacked and every field is now a column (as expected)
spark_reviews_DF = reviews_df.select("reviews.*")

spark_lookup_DF=spark.createDataFrame(lookup_df)

spark_reviews_DF = spark_reviews_DF.join(spark_lookup_DF, spark_reviews_DF.hotel_id == spark_lookup_DF.booking_id) \
                                   .select(prepare_spark_select_fields(interesting_fields))

# Start running the query that prints the running counts to the console
#spark_reviews_DF.printSchema()

actual_percentage += 4
now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = MSG_PROCESSING_COMPLETE
print(now+": "+msg)
log = format_log_message(msg = msg,
                        percentage = actual_percentage,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

#repartition(1) specifies in how many files spark saves the data, in this case we want just 1 file
query = spark_reviews_DF \
    .repartition(1) \
    .writeStream \
    .format("csv") \
    .option("path", output_dir) \
    .option("checkpointLocation", output_dir) \
    .option("header", True) \
    .outputMode("append") \
    .start()

#detector for the end of the queue
stop_stream_query(query,5)
actual_percentage += 4 #98%

try:
    rename_output_files(output_dir,make_output_file_name(config["outputs"][0][0]["config"]["entity"]))
    remove_output_files(output_dir)
except Exception as e:
    print(str(e))
    log = format_log_message(msg = str(e),
                            percentage = actual_percentage+1,
                            status = PIPELINE_LOGGER_STATUS_ERROR,
                            ts = int(time.time()*1000))
    send_log(log)

now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
msg = MSG_INGESTION_COMPLETE
print(now+": "+msg)
log = format_log_message(msg = msg,
                        percentage = actual_percentage,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

msg = str(total_extractions)+MSG_NUM_RECORDS_INGESTED
print(now+": "+msg)
log = format_log_message(msg = msg,
                        percentage = actual_percentage,
                        status = STATUS_RUNNING,
                        ts = int(time.time()*1000))
send_log(log)

#detele topic
try:
    delete_topic()
except Exception as e:
    print("Unable to delete broker topic")
finally:
    now = str(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    msg = now+": "+PIPELINE_LOGGER_MESSAGE_STATUS_END
    print(msg)
    log = format_log_message(msg = PIPELINE_LOGGER_MESSAGE_STATUS_END,
                            percentage = 100,
                            status = PIPELINE_LOGGER_STATUS_END,
                            ts = int(time.time()*1000))
    send_log(log)
    spark.stop()
    sys.exit(0)
